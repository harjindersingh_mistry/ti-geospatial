################################################################################
# wrapper for logging
################################################################################
import logging
from logging.handlers import WatchedFileHandler

class Logger:
    def __init__(self, module_name, log_file_path, level='info'):
        
        level = logging.DEBUG if level == 'debug' else logging.INFO

        self.logger = logging.getLogger(module_name)
        self.logger.setLevel(level)

        # create file, formatter and add it to the handlers
        fh = WatchedFileHandler(log_file_path)
        fh.setLevel(level)
        formatter = logging.Formatter(
            '%(asctime)s > %(funcName)s (%(lineno)d): %(message)s')
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)
