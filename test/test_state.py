
import os
import fiona
from lib.geo_data import GeoData


cur_dir = os.path.dirname(os.path.realpath(__file__))

state_file = os.path.join(cur_dir, *['data', 'state', 'cb_2017_us_state_500k.shp'])
state_polygons = fiona.open(state_file)
list_state_polygons = [p for p in state_polygons]

county_file = os.path.join(cur_dir, *['data', 'county', 'cb_2017_us_county_20m.shp'])
county_polygons = fiona.open(county_file)
list_county_polygons = [p for p in county_polygons]

urban_area_file = os.path.join(cur_dir, *['data', 'urban_area', 'cb_2017_us_ua10_500k.shp'])
urban_area_polygons = fiona.open(urban_area_file)
list_urban_area_polygons = [p for p in urban_area_polygons]

geo_data = GeoData(list_state_polygons, list_county_polygons, list_urban_area_polygons)


def test_state():
    state = geo_data.query_state(-81.632622, 38.349819)
    assert state == 'West Virginia'


def test_county():
    county = geo_data.query_county(-81.632622, 38.349819)
    assert county == 'Kanawha'


def test_urban_area():
    urban_area = geo_data.query_urban_area(-81.632622, 38.349819)
    assert urban_area == 'Charleston, WV'
