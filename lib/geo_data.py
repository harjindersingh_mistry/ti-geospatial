
import os
import time
import sys
import rtree
from shapely.geometry import Point, shape

import fiona
import boto3
import botocore
import zipfile


class GeoData:

    def __init__(self, conf):
        self.conf = conf
        self.list_state_polygons = None
        self.state_rtree_index = None

        self.list_county_polygons = None
        self.county_rtree_index = None

        self.list_urban_area_polygons = None
        self.urban_area_rtree_index = None

        self.list_zip_code_polygons = None
        self.zip_code_rtree_index = None

    def _extract_shapes(self, info_key):
        bucket = self.conf['shape_files']['bucket']
        key = self.conf['shape_files'][info_key]
        try:
            # download zip file from s3
            print("Downloading shape file for {0} from s3".format(info_key))
            if not os.path.exists('/tmp/' + info_key):
                os.makedirs('/tmp/' + info_key)
            s3 = boto3.resource('s3')
            # s3.Bucket(bucket).download_file(key, '/tmp/' + info_key + 'shapefiles.zip')

            # unzip file
            print("Processing shape file for {0}".format(info_key))
            zip_ref = zipfile.ZipFile('/tmp/' + info_key + 'shapefiles.zip', 'r')
            zip_ref.extractall('/tmp/' + info_key)
            zip_ref.close()

            # find shape file
            shape_file = None
            for file in os.listdir("/tmp/" + info_key):
                if file.endswith(".shp"):
                    shape_file = os.path.join("/tmp/" + info_key, file)
                    break

            # read shapefile using fiona
            if shape_file is not None:
                polygons = fiona.open(shape_file)
                print("Extracted polygons from shape file for {0}".format(info_key))
                print("Size of polygons {0}".format(sys.getsizeof(polygons)))
                cnt = 0
                for p in polygons:
                    cnt += 1
                    print("polygon: {0}".format(cnt))
                list_output = [p for p in polygons]
                print("Size of polygons {0}".format(sys.getsizeof(list_output)))
                return list_output
            else:
                print("Something went wrong! Could not extract polygons from shape file for {0}".format(info_key))
                return list()  # i.e. empty list

        except botocore.exceptions.ClientError as e:
            print("Error {0} occurred while reading S3 file!".format(e.response['Error']['Code']))
            raise

    def _build_index(self, list_polygons):
        print("Building index")
        list_bounds = [fiona.bounds(b) for b in list_polygons]
        stream = ((i, bounds, None) for i, bounds in enumerate(list_bounds))
        start = time.time()
        output = rtree.index.Index(stream)
        end = time.time()
        print("Index is ready in {0} seconds!".format(end-start))
        return output

    def _query(self, lat, lng, list_polygons, index, prop='NAME'):
        if lat is None or lng is None:
            return "Unknown"
        elif type(lat) == unicode and not lat.isnumeric:
            return "Unknown"
        elif type(lng) == unicode and not lng.isnumeric:
            return "Unknown"

        try:
            list_bound_ids = index.intersection((float(lat), float(lng)))
            query = Point(float(lat), float(lng))
            list_bound_ids = filter(lambda x: query.intersects(shape(list_polygons[x]['geometry'])),
                                list_bound_ids)

            if len(list_bound_ids) <= 0:
                return "Unknown"
            else:
                bound_id = list_bound_ids[0]
                print(list_polygons[bound_id]['properties'])
                return list_polygons[bound_id]['properties'][prop]

        except:
            print("Lat: ", lat, "Lng: ", lng)
            print("Oops! ", sys.exc_info()[0], " occurred.")
            return "Unknown"

    def query_state(self, lat, lng):
        if self.state_rtree_index is None:
            self.list_state_polygons = self._extract_shapes('state')
            self.state_rtree_index = self._build_index(self.list_state_polygons)

        return self._query(lat, lng,
                           self.list_state_polygons, self.state_rtree_index)

    def query_county(self, lat, lng):
        if self.county_rtree_index is None:
            self.list_county_polygons = self._extract_shapes('county')
            self.county_rtree_index = self._build_index(self.list_county_polygons)

        return self._query(lat, lng,
                           self.list_county_polygons, self.county_rtree_index)

    def query_urban_area(self, lat, lng):
        if self.urban_area_rtree_index is None:
            self.list_urban_area_polygons = self._extract_shapes('urban_area')
            self.urban_area_rtree_index = self._build_index(self.list_urban_area_polygons)

        return self._query(lat, lng,
                           self.list_urban_area_polygons, self.urban_area_rtree_index,
                           prop='NAME10')

    def query_zip_code(self, lat, lng):
        if self.zip_code_rtree_index is None:
            self.list_zip_code_polygons = self._extract_shapes('zip_code')
            print("List of zip code polygons is ready!")
            self.zip_code_rtree_index = self._build_index(self.list_zip_code_polygons)

        return self._query(lat, lng,
                           self.list_zip_code_polygons, self.zip_code_rtree_index,
                           prop='ZCTA5CE10')
