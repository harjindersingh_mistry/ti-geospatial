# ti-geospatial
 by: Bharat Jain & Harjindersingh Mistry, SmartDrive Systems       

## Project Docs

## Synopsis
This script augments the input data with geo-spatial information like state, county, zip-code and urban area.
For mapping a (latitude, longitude) pair to geo-spatial location information, it depends on various Shape files
published by the US Census department [1].

## References:
[1] https://www.census.gov/geo/maps-data/data/tiger-cart-boundary.html

## Usage:
The main code "main.py" is a PySpark job, which can be used as follows:
```
spark-submit main.py --state-file <state-shape-file> \
--county-file <county-shape-file> \
--zip-code-file <zip-code-shape-file> \
--urban-area-file <urban-area-shape-file> \
--input-file <CSV file with at least latitude and longitude columns>
```
