
import os
import sys
import yaml
from datetime import datetime
import argparse


from pyspark.sql import SparkSession
from pyspark.sql.functions import udf
from pyspark.sql.types import StringType

# add project lib to path
PATH = os.path.dirname(os.path.realpath(__file__))
sys.path.append(PATH + '/lib')
from lib.logger import Logger
from lib.geo_data import GeoData


# Global variables initiation
CONF_FILE = PATH + '/conf/geospatial.yaml'
with open(CONF_FILE, 'r') as config_file:
    CONF = yaml.load(config_file.read())

DATETIME_FMT = CONF['datetime_fmt']
LOG_NAME = CONF['logging']['log_file_path'] + CONF['logging']['log_file_prefix'] +\
            datetime.strftime(datetime.now(), DATETIME_FMT) + '.log'

# global logger
LOGGER = Logger(CONF['logging']['module_name'], PATH + LOG_NAME).logger


def find_state_name(lat, lng):
    geo_data = gbl_geo_data.value
    return geo_data.query_state(lat, lng)


def find_county_name(lat, lng):
    geo_data = gbl_geo_data.value
    return geo_data.query_county(lat, lng)


def find_urban_area_name(lat, lng):
    geo_data = gbl_geo_data.value
    return geo_data.query_urban_area(lat, lng)


def find_zip_code(lat, lng):
    geo_data = gbl_geo_data.value
    return geo_data.query_zip_code(lat, lng)


if __name__ == "__main__":
    parser = argparse.ArgumentParser("Run TI-Geo-Spatial Job.")
    parser.add_argument('--input-file', type=str, required=True, dest='input_file',
                        help='S3 Parquet file that contains input rows.')
    parser.add_argument('--latitude-column', type=str, required=True, dest='latitude_column',
                        help='Name of column that contains latitude information.')
    parser.add_argument('--longitude-column', type=str, required=True, dest='longitude_column',
                        help='Name of column that contains longitude information.')
    parser.add_argument('--outut-file', type=str, required=False, dest='output_file',
                        help='S3 Parquet file that will store the output.')
    args = parser.parse_args()

    # Run main function to calculate sun phases
    LOGGER.info("\n\n --- Starting --- \n\n")
    try:
        spark = SparkSession.builder.appName("TI-Geo-Spatial").getOrCreate()

        geo_data = GeoData(conf=CONF)
        gbl_geo_data = spark.sparkContext.broadcast(geo_data)

        state_udf = udf(find_state_name, StringType())
        county_udf = udf(find_county_name, StringType())
        urban_area_udf = udf(find_urban_area_name, StringType())
        zip_code_udf = udf(find_zip_code, StringType())

        LOGGER.info("Reading and augmenting the given data with state, county and urban-area information...")
        df_input = spark.read.load(args.input_file)
        # df_input = spark.read.format("csv").option("header", "true").load("C:\\Users\\harjindersingh.mistr\\Downloads\\truck_lat_lng.txt")
        df = df_input \
            .withColumn("state", state_udf(args.longitude_column, args.latitude_column))\
            .withColumn("county", county_udf(args.longitude_column, args.latitude_column))\
            .withColumn("urban_area", urban_area_udf(args.longitude_column, args.latitude_column))\
            .withColumn("zip_code", zip_code_udf(args.longitude_column, args.latitude_column))

        if args.output_file is not None:
            LOGGER.info("Writing output to the given S3 Parquet file...")
            df.write.parquet(args.output_file)
        else:
            LOGGER.info("Displaying output...")
            df.show()

    except:
        LOGGER.exception("Unexpected error occurred!")
        raise